@extends('layouts.master')

@section('content')
    <div id="main" role="main">
        <div id="ribbon">
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Tables</li>
            </ol>
        </div>
        <div id="content" style="opacity: 1;">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                    <h1 class="page-title txt-color-blueDark">
                        Редактирование заказов
                    </h1>
                </div>
            </div>
            <section>
                <div class="row">
                    <article class="col-sm-12 col-md-12 col-lg-12 sortable-grid ui-sortable">
                        <div class="jarviswidget">
                            <header class="ui-sortable-handle">
                                <span class="widget-icon">
                                    <i class="fa fa-edit"></i>
                                </span>
                                <h2>Редактирование заказа № {{ $order->id }}</h2>
                            </header>
                            <div role="content">
                                <div class="widget-body no-padding">
                                    <form action="/change" method="post" class="smart-form">
                                        {{ csrf_field() }}
                                        <header>Редактирование основной информации</header>
                                        <fieldset>
                                            <div class="row">
                                                <section class="col col-6 hide">
                                                    <label class="label" for="id">id</label>
                                                    <label class="input">
                                                        <i class="icon-append fa fa-envelope"></i>
                                                        <input type="text" name="id" id="id" value="{{ $order->id }}">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label" for="client_email">E-mail клиента</label>
                                                    <label class="input">
                                                        <i class="icon-append fa fa-envelope"></i>
                                                        <input type="email" name="client_email" id="client_email" required value="{{ $order->client_email }}">
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Партнер</label>
                                                    <label class="select">
                                                        <select name="partner_id" id="partner_id" class="{{ $order->id }}">
                                                            @foreach($partners as $partner)
                                                                <option value="{{ $partner->id }}" @if($partner->id == $order->partner->id) selected @endif>{{ $partner->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <i></i>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="label" for="status">Статус заказа</label>
                                                    <label class="select">
                                                        <select name="status">
                                                            @foreach(array_keys($orderStatuses) as $status)
                                                                <option value="{{ $status }}" @if($status == $order->status) selected @endif>{{ $orderStatuses[$status] }}</option>
                                                            @endforeach
                                                        </select> <i></i>
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Стоимость заказа</label>
                                                    <h2 id="total_price">{{ $order->orderTotalPrice() }} руб.</h2>
                                                </section>
                                            </div>
                                            <br/>
                                            <h5>Редактирование товаров, включенных в заказ</h5>
                                            <br/>
                                            <div class="row" id="add-product">
                                                <section class="col col-6">
                                                @foreach($order->orderProduct as $orderProduct)
                                                    <label class="label" id="label_{{ $orderProduct->id }}">Продукт</label>
                                                    <label class="select">
                                                        <select class="destroy_product" id="{{ $orderProduct->id }}">
                                                            <option value="1" selected>{{ $orderProduct->product->name }} ({{ $orderProduct->quantity }} шт.)</option>
                                                            <option value="0">Удалить продукт</option>
                                                        </select> <i id="some_strange_things_{{ $orderProduct->id }}"></i>
                                                    </label>
                                                @endforeach
                                                </section>
                                            </div>
                                            <br/>
                                            <h5>Добавить товар в заказ</h5>
                                            <br/>
                                            <div class="row">
                                                <section class="col col-6">
                                                    <label class="select">
                                                        <select name="add-product" id="add_product" class="{{ $order->id }}">
                                                            <option>Добавить продукт</option>
                                                            @foreach($products as $product)
                                                                <option value="{{ $product->id }}">Добавить {{ $product->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <i></i>
                                                    </label>
                                                </section>
                                            </div>
                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary">Сохранить</button>
                                        </footer>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
        </div>
    </div>
@endsection