@component('mail::message')
# Статус заказа {{ $messages->order_id }} "завершен"

Список заказа
@foreach($messages->products as $product)
- {{ $product }}
@endforeach

Общая сумма заказа
{{ $messages->total_price }}
Спасибо,<br>
{{ config('app.name') }}
@endcomponent
