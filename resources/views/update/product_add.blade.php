<label class="label" id="{{ $orderProduct->id }}">Продукт</label>
<label class="select">
    <select class="destroy_product" id="label_{{ $orderProduct->id }}">
        <option value="1" selected>{{ $orderProduct->product->name }} ({{ $orderProduct->quantity }} шт.)</option>
        <option value="0">Удалить продукт</option>
    </select> <i></i>
</label>