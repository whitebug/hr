@extends('layouts.master')

@section('content')
    <div id="main" role="main">
        <div id="ribbon">
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Tables</li>
            </ol>
        </div>
        <div id="content" style="opacity: 1;">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                    <h1 class="page-title txt-color-blueDark">
                        Список продуктов
                    </h1>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                </div>
            </div>
            <section>
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-darken jarviswidget-sortable">
                            <header class="ui-sortable-handle">
                                <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
                            <div>
                                <div class="jarviswidget-editbox">
                                </div>
                                <div class="widget-body no-padding">

                                    <div id="dt_basic_wrapper fixed-table-header"
                                         class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                        <div class="dt-toolbar">
                                            <div class="col-xs-12 col-sm-6">

                                            </div>
                                            <div class="col-sm-6 col-xs-12 hidden-xs">
                                                <div class="dataTables_length" id="dt_basic_length">
                                                    <label>
                                                        Заказов на странице
                                                        <select class="form-control input-sm pagination_select">
                                                            @foreach($pageNumArray as $pageNum)
                                                                @if($pageNum!=$pages)
                                                                    <option value="{{ route('products',array_merge($input,['pages'=>$pageNum])) }}">{{ $pageNum }}</option>
                                                                @else
                                                                    <option value="{{ route('products',array_merge($input,['pages'=>$pageNum])) }}" selected>{{ $pageNum }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting" title="id">ID</th>
                                                <th class="sorting" title="name">
                                                    <i class="fa fa-fw fa-gift"></i>
                                                    Наименование продукта
                                                </th>
                                                <th class="sorting" title="vendor_id">
                                                    <i class="fa fa-fw fa-gift"></i>
                                                    <i class="fa fa-fw fa-user"></i>
                                                    Наименование поставщика
                                                </th>
                                                <th class="sorting" title="price">
                                                    <i class="fa fa-fw fa-dollar-sign"></i>
                                                    Цена
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($products as $product)
                                                <form action="/price" method="post">
                                                    {{ csrf_field() }}
                                                    <tr>
                                                        <td role="gridcell">
                                                            {{ $product->id }}
                                                        </td>
                                                        <td>
                                                            {{ $product->name }}
                                                        </td>
                                                        <td>
                                                            {{ $product->vendor->name }}
                                                        </td>
                                                        <td>
                                                            <input type="text" style="display: none" name="id" id="id" required value="{{ $product->id }}">
                                                            <label for="id" style="display: none"></label>
                                                            <input type="text" name="price" id="price" required value="{{ $product->price }}">
                                                            <label for="price"> руб.</label>
                                                            <button class="btn btn-xs btn-default btn_price_edit" title="{{ $product->id }}">
                                                                <i class="fa fa-sign-in-alt"></i>
                                                                Сохранить
                                                            </button>
                                                        </td>
                                                    </tr>
                                                </form>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <div class="dt-toolbar-footer">
                                            <div class="col-sm-6 col-xs-12 hidden-xs">
                                                <div class="dataTables_info" id="dt_basic_info" role="status"
                                                     aria-live="polite">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="dataTables_paginate paging_simple_numbers"
                                                     id="dt_basic_paginate">
                                                    <ul class="pagination">
                                                        {{ $products->links() }}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
        </div>
    </div>
@endsection