@extends('layouts.master')

@section('content')
    <div id="main" role="main">
        <div id="ribbon">
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Tables</li>
            </ol>
        </div>
        <div id="content" style="opacity: 1;">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                    <h1 class="page-title txt-color-blueDark">
                        <i class="fa fa-lg fa-fw fa-sun"></i>
                        Погода в Брянске
                    </h1>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                </div>
            </div>
            <section>
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget">
                            <div class="no-padding">
                                <div class="padding-10-no-top no-padding-bottom">
                                    <div class="show-stat-microcharts">
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <img src="{{ $weather['observations']['location']['0']['observation']['0']['iconLink']}}">
                                            <div class="easy-pie-chart">
                                                <canvas height="50" width="50"></canvas>
                                            </div>
                                            <span class="title">
                                                {{$weather['observations']['location']['0']['observation']['0']['temperature']}}°C
                                            </span>
                                            <ul class="smaller-stat pull-right">
                                                <li>
                                                    <span class="label bg-color-red">
                                                        <i class="fa fa-caret-up"></i>
                                                        {{$weather['observations']['location']['0']['observation']['0']['highTemperature']}}°C
                                                    </span>
                                                </li>
                                                <li>
                                                    <span class="label bg-color-blueLight">
                                                        <i class="fa fa-caret-down"></i>
                                                        {{$weather['observations']['location']['0']['observation']['0']['lowTemperature']}}°C
                                                    </span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="easy-pie-chart">
                                                <span class="percent percent-sign">
                                                    {{$weather['observations']['location']['0']['observation']['0']['humidity']}}
                                                </span>
                                                <canvas height="50" width="120"></canvas>
                                            </div>
                                            <span class="title"> Влажность </span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="easy-pie-chart">
                                                <span class="percent mm-sign">
                                                    {{$weather['observations']['location']['0']['observation']['0']['barometerPressure']}}
                                                </span>
                                                <canvas height="50" width="120"></canvas></div>
                                            <span class="title"> Атмосферное давление </span>
                                        </div>
                                        <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                            <div class="easy-pie-chart">
                                                <span class="percent meter-sign">
                                                    {{$weather['observations']['location']['0']['observation']['0']['windSpeed']}}
                                                </span>
                                                <canvas height="50" width="100"></canvas></div>
                                            <span class="title"> Скорость ветра </span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
        </div>
    </div>
@endsection