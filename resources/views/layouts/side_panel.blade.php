<aside id="left-panel">
    <div class="login-info">
        <span>
            <div>
                <img src="{{ asset('img/avatar/sodnomov.png') }}" alt="me" class="online">
                <span>
                    Алексей Содномов
                </span>
            </div>
        </span>
    </div>
    <nav>
        <ul class="nav nav-pills">
            <li class="nav-item">
                <a href="/" title="Home" class="nav-link active">
                    <i class="fa fa-lg fa-fw fa-sun"></i>
                    <span class="menu-item-parent">Погода в Брянске</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="/table" title="Table" class="nav-link">
                    <i class="fa fa-lg fa-fw fa-list-ol"></i>
                    <span class="menu-item-parent">Список заказов</span>
                </a>
            </li>
            <li class="nav-item">
                <a href="/products" title="Products" class="nav-link">
                    <i class="fa fa-lg fa-fw fa-balance-scale"></i>
                    <span class="menu-item-parent">Список продуктов</span>
                </a>
            </li>
        </ul>
    </nav>
</aside>