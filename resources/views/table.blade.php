@extends('layouts.master')

@section('content')
    <div id="main" role="main">
        <div id="ribbon">
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Tables</li>
            </ol>
        </div>
        <div id="content" style="opacity: 1;">
            <div class="row">
                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
                    <h1 class="page-title txt-color-blueDark">
                        Список заказов
                    </h1>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
                </div>
            </div>
            <section>
                <div class="row">
                    <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <div class="jarviswidget jarviswidget-color-darken jarviswidget-sortable">
                            <header role="heading" class="ui-sortable-handle">

                                <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
                            <div>
                                <div class="jarviswidget-editbox">
                                </div>
                                <div class="widget-body no-padding">

                                    <div id="dt_basic_wrapper fixed-table-header"
                                         class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                        <div class="dt-toolbar">
                                            <div class="col-xs-12 col-sm-6">

                                            </div>
                                            <div class="col-sm-6 col-xs-12 hidden-xs">
                                                <div class="dataTables_length" id="dt_basic_length">
                                                    <label>
                                                        Заказов на странице
                                                        <select class="form-control input-sm pagination_select">
                                                            @foreach($pageNumArray as $pageNum)
                                                                @if($pageNum!=$pages)
                                                                    <option value="{{ route('table',array_merge($input,['pages'=>$pageNum])) }}">{{ $pageNum }}</option>
                                                                @else
                                                                    <option value="{{ route('table',array_merge($input,['pages'=>$pageNum])) }}" selected>{{ $pageNum }}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <header class="ui-sortable-handle">
                                            <ul class="nav nav-tabs pull-right in" id="myTab">
                                                <li class="tab_active active">
                                                    <a data-toggle="tab" class="tab_selector" id="all" aria-expanded="false">
                                                        <i class="fa fa-globe-asia"></i>
                                                        <span class="hidden-mobile hidden-tablet">
                                                            Все заказы
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="tab_active">
                                                    <a data-toggle="tab" class="tab_selector" id="exceed" aria-expanded="false">
                                                        <i class="fa fa-clock"></i>
                                                        <span class="hidden-mobile hidden-tablet">
                                                            Пророченные
                                                        </span>
                                                    </a>
                                                </li>

                                                <li class="tab_active">
                                                    <a data-toggle="tab" class="tab_selector" id="current" aria-expanded="true">
                                                        <i class="fa fa-gift"></i>
                                                        <span class="hidden-mobile hidden-tablet">
                                                            Текущие
                                                        </span></a>
                                                </li>

                                                <li class="tab_active">
                                                    <a data-toggle="tab" class="tab_selector" id="new">
                                                        <i class="fa fa-newspaper"></i>
                                                        <span class="hidden-mobile hidden-tablet">
                                                            Новые
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="tab_active">
                                                    <a data-toggle="tab" class="tab_selector" id="delivered">
                                                        <i class="fa fa-dollar-sign"></i>
                                                        <span class="hidden-mobile hidden-tablet">
                                                            Выполненные
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>

                                            <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i>
                                            </span>
                                        </header>
                                        <div id="myTabContent" class="tab-content">
                                            <table class="table table-striped table-bordered table-hover dataTable no-footer">
                                                <thead>
                                                <tr role="row">
                                                    <th class="sorting" title="id">ID</th>
                                                    <th data-class="expand" class="expand sorting" title="partner_id">
                                                        <i class="fa fa-fw fa-user"></i>
                                                        Название партнера
                                                    </th>
                                                    <th>
                                                        <i class="fa fa-fw fa-dollar-sign"></i>
                                                        Стоимость заказа
                                                    </th>
                                                    <th colspan="3">
                                                        <i class="fa fa-fw fa-gift"></i>
                                                        Наименование и состав заказа
                                                    </th>
                                                    <th class="sorting" title="status">
                                                        <i class="fa fa-fw fa-route"></i>
                                                        Статус заказа
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($orders as $order)
                                                    @php($first = true)
                                                    @foreach($order->orderProduct as $product)
                                                        <tr>
                                                            @if ($first)
                                                                <td rowspan="{{ $order->productNumber() }}" role="gridcell">
                                                                    {{ $order->id }}
                                                                    {{--<button class="btn btn-xs btn-default btn-edit" onclick="jQuery('#jqgrid').editRow('1');">--}}
                                                                    <button class="btn btn-xs btn-default btn-edit" id="{{ $order->id }}">
                                                                        <i class="fa fa-edit"></i>
                                                                    </button>
                                                                </td>
                                                            @endif
                                                            @if ($first)
                                                                <td rowspan="{{ $order->productNumber() }}">
                                                                    {{ $order->partner->name }}
                                                                </td>
                                                            @endif
                                                            @if ($first)
                                                                <td rowspan="{{ $order->productNumber() }}">
                                                                    {{ $order->orderTotalPrice() }} руб.
                                                                </td>
                                                            @endif
                                                            <td>{{ $product->product->name }}</td>
                                                            <td>{{ $product->quantity }} шт.</td>
                                                            <td>{{ $product->price }} руб.</td>
                                                            @if ($first)
                                                                <td rowspan="{{ $order->productNumber() }}" class="{{ $order->status()['class'] }}">{{ $order->status()['title'] }}</td>
                                                                @php($first = false)
                                                            @endif
                                                        </tr>
                                                    @endforeach
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>

                                        <div class="dt-toolbar-footer">
                                            <div class="col-sm-6 col-xs-12 hidden-xs">
                                                <div class="dataTables_info" id="dt_basic_info" role="status"
                                                     aria-live="polite">
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6">
                                                <div class="dataTables_paginate paging_simple_numbers"
                                                     id="dt_basic_paginate">
                                                    <ul class="pagination">
                                                        {{ $orders->links() }}
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
            </section>
        </div>
    </div>
@endsection