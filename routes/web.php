<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Погода в Брянске
Route::get('/','MainPageController@index');
//Заказы
Route::get('table','MainPageController@table')->name('table');
//Продукты
Route::get('products','ProductController@index')->name('products');
//Редактирование
Route::get('edit/{item}','EditOrderController@edit')->name('edit');
//Добавить продукт к заказу
Route::get('update','EditOrderController@update');
//Удалить продукт из заказа
Route::get('destroy','EditOrderController@destroy');
//Сохранение редактированного заказа
Route::post('change','EditOrderController@change');
//Сохранение редактированной цены
Route::post('price','ProductController@price');