<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailMessage extends Model
{
    protected $casts = [
        'products' => 'json',
    ];
    protected $fillable = ['order_id','products','total_price','user'];
}
