<?php

namespace App\Http\Controllers\Emails;

use App\EmailMessage;
use App\Http\Controllers\Controller;
use App\Jobs\SendEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class EmailController extends Controller
{
    public function mainEmail(Request $request){
        $rules = [
            'user' => 'required',
        ];

        $validator = Validator::make($request->all(),$rules);

        if($validator->errors()!='[]'){
            return view('layouts.ajax.errors')->withErrors($validator->errors());
        }else{
            //signalize that all the things is ok
            $response_text='OK';
            //create an application
            $messages = EmailMessage::create(request(['order_id','products','total_price']));

            $users = $request->user;

            foreach($users as $user){
                $emailJob = (new SendEmail($messages,$user))->delay(Carbon::now()->addSeconds(3));
                dispatch($emailJob);
            }
        }

        return response($response_text, 200)
            ->header('Content-Type', 'text/plain');
    }
}
