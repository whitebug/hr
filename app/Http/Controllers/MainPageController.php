<?php

namespace App\Http\Controllers;


use App\Order;
use GuzzleHttp\Client;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;

class MainPageController extends Controller
{
    public function index(){

        $minutes = 60;
        $weather = Cache::remember('forecast', $minutes, function () {
            $app_id = config("services.app_id");
            $app_code = config("services.app_code");
            $city = 'Brjansk';
            $url = "https://weather.api.here.com/weather/1.0/report.json?app_id=${app_id}&app_code=${app_code}&product=observation&name=${city}";
            $client = new Client();
            $res = $client->get($url);
            if ($res->getStatusCode() == 200) {
                $result = json_decode($res->getBody(), true);
            }else{
                $result = [];
            }
            return $result;
        });
        return view('index')->with(['weather' => $weather]);
    }


    public function table(){
        $orders = new Order;
        $queries = [];
        //фильтры
        $filters = ['id','partner_id','status'];
        //страниц по умолчанию
        $pages = 5;
        if(request()->has('pages')){
            $pages = request('pages');
            $queries['pages'] = request('pages');
        }

        //применение фильтров
        foreach($filters as $filter){
            if(request()->has($filter)){
                $orders = $orders->orderBy($filter,request($filter));
                $queries[$filter] = request($filter);
            }
        }
        //просроченные
        if(request()->has('exceed')){
            $orders = $orders->where('delivery_dt','<',Carbon::today()->toDateString())->where('status','10')->orderBy('delivery_dt','desc')->take(50);
            $queries['exceed'] = request('exceed');
        }
        //текущие
        if(request()->has('current')){
            $orders = $orders->where('delivery_dt','<',Carbon::parse('-24 hours'))->where('status','10')->orderBy('delivery_dt','asc');
            $queries['exceed'] = request('exceed');
        }
        //новые
        if(request()->has('new')){
            $orders = $orders->where('delivery_dt','>',Carbon::today()->toDateString())->where('status','0')->orderBy('delivery_dt','asc')->take(50);
            $queries['exceed'] = request('exceed');
        }
        //выполненные
        if(request()->has('delivered')){
            $orders = $orders->where('delivery_dt','>=',Carbon::today()->toDateString())->where('status','20')->orderBy('delivery_dt','desc')->take(50);
            $queries['exceed'] = request('exceed');
        }

        $orders = $orders->paginate($pages)->appends($queries);
        //варианты выбора количества страниц
        $pageNumArray = array('5','10','25','50');
        //переменные ввода
        $input = Input::all();
        return view('table')
            ->with(['orders'=>$orders,'pageNumArray'=>$pageNumArray,'pages'=>$pages,'input'=>$input]);
    }


}
