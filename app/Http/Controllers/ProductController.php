<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePriceRequest;
use App\Product;
use Illuminate\Support\Facades\Input;

class ProductController extends Controller
{

    /**
     * Базовая страница продуктов
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $products = new Product;
        $queries = [];
        //фильтры для сортировки
        $filters = ['name','price','brands','vendor'];
        //страниц по умолчанию
        $pages = 25;
        //смена количества страниц
        if(request()->has('pages')){
            $pages = request('pages');
            $queries['pages'] = request('pages');
        }
        //применение фильтров
        foreach($filters as $filter){
            if(request()->has($filter)){
                $products = $products->orderBy($filter,request($filter));
                $queries[$filter] = request($filter);
            }
        }

        $products = $products->paginate($pages)->appends($queries);
        //варианты выбора количества страниц
        $pageNumArray = array('5','10','25','50');
        //get переменные
        $input = Input::all();
        return view('products')
            ->with(['products'=>$products,'pageNumArray'=>$pageNumArray,'pages'=>$pages,'input'=>$input]);
    }

    /**
     * Изменение цены продукта на странице продуктов
     * @param ChangePriceRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function price(ChangePriceRequest $request){
        $product = Product::whereId($request->id)->firstOrFail();
        $product->price = $request->price;
        $product->save();
        return back()->withInput(Input::all());
    }
}
