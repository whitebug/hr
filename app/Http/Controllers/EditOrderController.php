<?php

namespace App\Http\Controllers;

use App\EmailMessage;
use App\Http\Requests\OrderChangeRequest;
use App\Http\Requests\OrderProductEditRequest;
use App\Jobs\SendEmail;
use App\Order;
use App\OrderProduct;
use App\Partner;
use App\Product;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;

class EditOrderController extends Controller
{
    protected function ifOrderProduct($order_id, $product_id){
        if($orderProduct = OrderProduct::where('order_id',$order_id)
            ->where('product_id',$product_id)->first()){

            return $orderProduct;
        }else{
            return false;
        }
    }

    public function edit($id){
        $products = Product::all();
        $orderStatuses = ['0'=>'новый','10'=>'подтвержден','20'=>'завершен'];
        $order = Order::whereId($id)->first();
        $partners = Partner::all();

        $input = Input::all();


        return view('edit')
            ->with(['order'=>$order,'orderStatuses'=>$orderStatuses,'products'=>$products,'input'=>$input,'partners'=>$partners]);
    }

    public function update(OrderProductEditRequest $request){
        if($request->has('order_id')){
            if($request->has('product_id')){
                if($orderProduct = $this->ifOrderProduct($request->order_id,$request->product_id)){
                    //Если продукт уже есть в заказе, увеличиваем его количество
                    $orderProduct->quantity += 1;
                    $orderProduct->save();
                }else{
                    //Если такого продукта нет, создаем новый
                    $orderProduct = new OrderProduct([
                    'order_id' => $request->order_id,
                    'product_id' => $request->product_id,
                    'quantity' => 1,
                    'price' => Product::whereId($request->product_id)->first()->price,
                    ]);
                    $orderProduct->save();
                }
                return back();
            }
            return back();
        }
        return back();
    }

    public function destroy(OrderProductEditRequest $request){
        if($request->has('order_product_id')){
            if($orderProduct = OrderProduct::whereId($request->order_product_id)->firstOrFail()){
                $orderProduct->delete();
                $totalPrice = $orderProduct->order->orderTotalPrice();
                return response()->json(['order_product_id'=>$request->order_product_id,'total_price'=>$totalPrice]);
            }else{
                return response()->json(['order_product_id'=>'error','total_price'=>'error']);
            }
        }
        return response()->json(['order_product_id'=>'error','total_price'=>'error']);
    }

    public function change(OrderChangeRequest $request){
        $order = Order::whereId($request->id)->firstOrFail();
        $order->status = $request->status;
        $order->client_email = $request->client_email;
        $order->partner_id = $request->partner_id;
        $order->save();
        if($request->status==20){
            $products = $order->orderProduct;
            $productNames = [];
            $emails = [];
            foreach($products as $product){
                array_push($productNames,$product->product->name);
                array_push($emails,$product->product->vendor->email);
            }
            array_push($emails,$order->client_email);
            $products = json_encode($productNames);
            $messages = EmailMessage::
            create(['products'=>$products,'order_id'=>$order->id,'total_price'=>$order->orderTotalPrice(),]);

            foreach($emails as $user){
                $emailJob = (new SendEmail($messages,$user))->delay(Carbon::now()->addSeconds(3));
                dispatch($emailJob);
            }
        }
        return redirect('/table');
    }
}