<?php

namespace App\Mail;

use App\EmailMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MainForm extends Mailable
{
    use Queueable, SerializesModels;
    public $messages;

    /**
     * Create a new message instance.
     *
     * @param EmailMessage $messages
     */
    public function __construct(EmailMessage $messages)
    {
        $this->messages = $messages;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Ваш заказ завершен')
            ->markdown('emails.main_form');
    }
}
