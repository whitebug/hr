<?php

namespace App\Jobs;

use App\Mail\MainForm;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $messages;
    protected $user;

    /**
     * Create a new job instance.
     *
     * @param $messages
     * @param $user
     */
    public function __construct($messages,$user)
    {
        $this->messages = $messages;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Mail::to($this->user)->send(new MainForm($this->messages));
    }
}
