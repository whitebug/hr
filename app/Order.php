<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * @property mixed orderProduct
 * @property mixed orderPrices
 * @property mixed status
 * @property mixed id
 */
class Order extends Model
{
    public function partner(){
        return $this->belongsTo(Partner::class);
    }

    public function orderProduct(){
        return $this->hasMany(OrderProduct::class);
    }

    public function orderTotalPrice(){
        return $this->orderProduct->sum(function ($orderProduct){
            return $orderProduct->quantity*$orderProduct->price;
        });
    }

    public function status(){
        switch ($this->status){
            case 0:
                return ['title'=>'новый','class'=>'info'];
                break;
            case 10:
                return ['title'=>'подтвержден','class'=>'warning'];
                break;
            case 20:
                return ['title'=>'завершен','class'=>'success'];
                break;
        }
    }

    public function productNumber(){
        return count($this->orderProduct->where('order_id',$this->id));
    }

}
