$(document).ready(function(){
    //Редактирование заказа
    $('.btn-edit').on('click',function(){
        var id = $(this).attr('id');
        var win = window.open('/edit/'+id, '_blank');
        if (win) {
            win.focus();
        } else {
            alert('Пожалуйста разрешите всплывающие окна');
        }
    });
    //Добавление продукта к заказу
    $('#add_product').change(function(){
        var productId = $(this).val();
        var orderId = $(this).attr('class');
        if(productId && orderId){
            window.location.href = "/update?order_id="+orderId+"&product_id="+productId;
        }
    });
    //Удаление продукта из заказа
    $('select.destroy_product').change(function(){
        var state = $(this).val();
        if(state==0){
            var productId = $(this).attr('id');
            $.ajax({
                type: 'GET',
                url : "/destroy?order_product_id="+productId,
                success : function (data) {
                    if(data['order_product_id']){
                        $("#label_"+data['order_product_id']).hide();
                        $("select#"+data['order_product_id']).hide();
                        $("#some_strange_things_"+data['order_product_id']).hide();
                    }
                    if(data['total_price']){
                        $("#total_price").text(data['total_price']+' руб.');
                    }

                }
            });
        }
    });
    //Сохранение цены
    $('.btn_price_edit').on('click',function(){
        var product = $(this).attr('title');
        var price = $('#price').val();
        $.post("/price",
            {
                id: product,
                price: price
            });
    });
    //Изменение числа предметов на странице
    $('.pagination_select').change(function(){
        var pages = $(this).val();
        if(pages){
            window.location.href = pages;
        }
    });
    //Сортировка
    var getUrlParameter = function getUrlParameter(sParam) {
        var sPageURL = window.location.search.substring(1),
            sURLVariables = sPageURL.split('&'),
            sParameterName,
            i;

        for (i = 0; i < sURLVariables.length; i++) {
            sParameterName = sURLVariables[i].split('=');

            if (sParameterName[0] === sParam) {
                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
            }
        }
    };
    $(document).on('click','.sorting',function(){
        $(this).removeClass('sorting');
        $(this).addClass('sorting_asc');
        var page = getUrlParameter('page');
        var pages = getUrlParameter('pages');
        if(!page)page='';
        if(!pages)pages='';
        var title = $(this).attr('title');
        window.location.href = window.location.pathname+'?pages='+pages+'&page='+page+'&'+title+'=asc';
    });

    //сортировка по вкладкам (просроченные, новые и т.д)
    $(document).on('click','.tab_selector',function () {
        var operation = $(this).attr('id');
        var page = getUrlParameter('page');
        var pages = getUrlParameter('pages');
        if(!page)page='';
        if(!pages)pages='';
        window.location.href = window.location.pathname+'?pages='+pages+'&page='+page+'&'+operation+'=1';
    });

    $.each(['all','exceed','current','new','delivered'],function(index,value){
        if(getUrlParameter(value)=='1'){
            $('.tab_active').removeClass('active');
            $('#'+value).parent().addClass('active');
        }
    })


});